__author__ = "BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.5"

import itertools

from resources.PytWork import IPv4

module_ip = IPv4()


class IPCalculator:
    """ [IPCalculator]

    FUNCTION LIST:

        :list: __init__(self):
                - ip_address
                - subnet_mask
                - number_hosts
                - number_network

        :list: network_binary(self)

        :list: subnet_mask_binary(self)
        :list: subnet_mask_size(self)

        :list: define_ip_class(self)

    """

    def __init__(self):
        self.ip_address = ip_address
        self.subnet_mask = subnet_mask

    def network_binary(self):
        return module_ip.convert_decimal_binary(module_ip.define_network(self.ip_address, self.subnet_mask))

    def subnet_mask_binary(self):
        return module_ip.convert_decimal_binary(self.subnet_mask)

    def subnet_mask_size(self):
        return module_ip.define_cidr(self.subnet_mask_binary())

    def define_ip_class(self):
        return module_ip.define_ip_class(self.network_binary())


class DivisionNumberHosts(IPCalculator):
    """ [DivisionNumberHosts] -> [IPCalculator:LEGACY]

    FUNCTION LIST:

        :list: number_bits_hosts_id(self)
        :list: number_bits_network_id(self)

        :list: new_subnet_mask_binary(self)
        :list: new_subnet_mask_size(self)

    """

    def __init__(self):
        super().__init__()
        self.number_hosts = number_hosts

    def number_bits_hosts_id(self):
        return module_ip.define_number_bits(self.number_hosts + 2)

    def number_bits_network_id(self):
        return 32 - module_ip.define_cidr(self.subnet_mask_binary()) - self.number_bits_hosts_id()

    def new_subnet_mask_binary(self):
        return module_ip.new_subnet_mask(self.subnet_mask_binary(), self.number_bits_network_id())

    def define_network_list(self):
        return module_ip.generate_network_id(self.network_binary(),
                                             self.subnet_mask_size(),
                                             self.number_bits_network_id())

    def new_addressing_binary(self):
        return [[address_binary + [int(bit) for bit in change] for change in
                 map("".join, itertools.product("01", repeat=self.number_bits_hosts_id()))] for address_binary in
                self.define_network_list()]

    def convert_decimal(self):
        network_binary, first_ip_address_binary, last_ip_address_binary, broadcast_address_binary = [], [], [], []

        for add in self.new_addressing_binary():
            network_binary.append(add[0])
            first_ip_address_binary.append(add[1])
            last_ip_address_binary.append(add[-2])
            broadcast_address_binary.append(add[-1])

        network_decimal = module_ip.convert_binary_decimal(network_binary)
        first_ip_address_decimal = module_ip.convert_binary_decimal(first_ip_address_binary)
        last_ip_address_decimal = module_ip.convert_binary_decimal(last_ip_address_binary)
        broadcast_address_decimal = module_ip.convert_binary_decimal(broadcast_address_binary)
        subnet_mask_decimal = sum(module_ip.convert_binary_decimal([self.new_subnet_mask_binary()]), [])

        """ VOIR A METTRE LE PRINT EN FONCTION DANS LE MODULE """

        for i in range(len(network_decimal)):
            print("""
                  Network: {}
                  Network address: {}.{}.{}.{}
                  First host address: {}.{}.{}.{}
                  Last host address: {}.{}.{}.{}
                  Broadcast address: {}.{}.{}.{}
                  Subnet-mask address: {}.{}.{}.{}
                  """.format(
                i + 1,
                *network_decimal[i],
                *first_ip_address_decimal[i],
                *last_ip_address_decimal[i],
                *broadcast_address_decimal[i],
                *subnet_mask_decimal, sep="."
            ))


class DivisionNumberNetwork(IPCalculator):
    def __init__(self):
        super().__init__()
        self.number_network = number_network

    def number_bits_network_id(self):
        return module_ip.define_number_bits(self.number_network)

    def number_bits_hosts_id(self):
        return 32 - module_ip.define_cidr(self.subnet_mask_binary()) - self.number_bits_network_id()

    def new_subnet_mask_binary(self):
        return module_ip.new_subnet_mask(self.subnet_mask_binary(), self.number_bits_network_id())

    def define_network_list(self):
        return module_ip.generate_network_id(self.network_binary(),
                                             self.subnet_mask_size(),
                                             self.number_bits_network_id())

    def new_addressing_binary(self):
        return [[address_binary + [int(bit) for bit in change] for change in
                 map("".join, itertools.product("01", repeat=self.number_bits_hosts_id()))] for address_binary in
                self.define_network_list()]

    def convert_decimal(self):
        network_binary, first_ip_address_binary, last_ip_address_binary, broadcast_address_binary = [], [], [], []

        for add in self.new_addressing_binary():
            network_binary.append(add[0])
            first_ip_address_binary.append(add[1])
            last_ip_address_binary.append(add[-2])
            broadcast_address_binary.append(add[-1])

        network_decimal = module_ip.convert_binary_decimal(network_binary)
        first_ip_address_decimal = module_ip.convert_binary_decimal(first_ip_address_binary)
        last_ip_address_decimal = module_ip.convert_binary_decimal(last_ip_address_binary)
        broadcast_address_decimal = module_ip.convert_binary_decimal(broadcast_address_binary)
        subnet_mask_decimal = sum(module_ip.convert_binary_decimal([self.new_subnet_mask_binary()]), [])

        """ VOIR A METTRE LE PRINT EN FONCTION DANS LE MODULE """

        for i in range(len(network_decimal)):
            print(""" Network: {}
                  Network address: {}.{}.{}.{}
                  First host address: {}.{}.{}.{}
                  Last host address: {}.{}.{}.{}
                  Broadcast address: {}.{}.{}.{}
                  Subnet-mask address: {}.{}.{}.{}
                  """.format(
                i + 1,
                *network_decimal[i],
                *first_ip_address_decimal[i],
                *last_ip_address_decimal[i],
                *broadcast_address_decimal[i],
                *subnet_mask_decimal, sep="."
            ))


class DivisionVLSM(IPCalculator):
    print(IPCalculator)


""" [PROGRAM] """

display = """
    Choice an option:
    \t1 : Division by number of hosts
    \t2 : Division by number of subnets
    \t3 : Division by variable number of hosts
    \t0 : Exit
    """

choice = "99"

while choice != "0":
    choice = input(display)
    match choice:
        case "1":
            ip_address = module_ip.say(str(input("Can you say IP Address : ")))
            subnet_mask = module_ip.say(str(input("Can you say Subnet mask : ")))
            number_hosts = int(input("Number hosts desired : "))

            DivisionNumberHosts.convert_decimal(DivisionNumberHosts())
        case "2":
            ip_address = module_ip.say(str(input("Can you say IP Address : ")))
            subnet_mask = module_ip.say(str(input("Can you say Subnet mask : ")))
            number_network = int(input("Number network desired : "))

            DivisionNumberNetwork.convert_decimal(DivisionNumberNetwork())
        case "3":
            ip_address = module_ip.say(str(input("Can you say IP Address : ")))
            subnet_mask = module_ip.say(str(input("Can you say Subnet mask : ")))
            number_network = int(input("Number network desired : "))

            print(DivisionVLSM())
