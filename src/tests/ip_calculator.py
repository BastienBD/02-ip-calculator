__author__ = "BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.5"

import re
import unittest


# peut-être passer la fonction say en fonction de saisie qui retourne la liste ip_address et cidr ou ip_address. Pour avoir une fonction qui récupère l'info dont elle a besoin
# si aucun masque n'est appliqué, alors l'application applique un masque par défaut en fonction de la classe de l'adresse IP saisie
# peut-être ajouter une expression régulière pour vérifier directement si le format est "X.X.X.X/X" ou "X.X.X.X".


class IpCalculator(unittest.TestCase):
    @staticmethod
    def say(add_ip_address, subnet_mask=None):
        pattern_1 = re.compile("([0-9]{1,3}\.){3}[0-9]{1,3}$")  # permet de vérifier ("192.168.10.10")
        pattern_2 = re.compile("([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2}$")  # permet de vérifier ("192.168.10.10/24")

        splitting = lambda ip_address: [int(byte) for byte in ip_address.split(".")]

        if re.match(pattern_1, add_ip_address):
            for value in splitting(add_ip_address):
                if 0 <= value <= 255:
                    if subnet_mask is not None:
                        return splitting(add_ip_address), splitting(subnet_mask)

                    return splitting(add_ip_address)

        elif re.match(pattern_2, add_ip_address):
            subnet_mask, add_ip_address = add_ip_address.split("/")[1], add_ip_address.split("/")[0]

            return splitting(add_ip_address), int(subnet_mask)

        else:
            return "Merci de saisir une adresse IP valide !"

    @staticmethod
    def convert_decimal_binary(add_ip_address_decimal):
        ip_address_binary = []

        for byte in add_ip_address_decimal:
            for bit in range(8):
                if byte - 2 ** (7 - bit) >= 0:
                    ip_address_binary.append(1)
                    byte -= 2 ** (7 - bit)
                else:
                    ip_address_binary.append(0)

        return ip_address_binary

    @staticmethod
    def convert_binary_decimal(add_ip_address_binary):
        ip_address_decimal = []
        cutting = lambda binary_address, bits: [binary_address[i: i + bits] for i in range(0, len(binary_address), bits)]

        for byte in cutting(add_ip_address_binary, 8):
            intermediate_variable = 0
            for bit in range(len(byte)):
                if byte[::-1][bit] == 1:
                    intermediate_variable += 2 ** bit

            ip_address_decimal.append(intermediate_variable)

        return ip_address_decimal

    @staticmethod
    def define_network(add_ip_address, add_subnet_mask):
        network_address = []

        for byte in range(len(add_ip_address)):
            network_address.append(add_ip_address[byte] & add_subnet_mask[byte])

        return network_address

    @staticmethod
    def define_class(add_network_binary):
        class_table = {"A": [0],
                       "B": [1, 0],
                       "C": [1, 1, 0],
                       "D": [1, 1, 1, 0],
                       "E": [1, 1, 1, 1, 0]}

        for enumerated, (key, value) in enumerate(class_table.items()):
            if value == add_network_binary[:len(value)]:
                return key

    """ [IP CALCULATOR]

    Lists all the tests necessary for the proper functioning of the application

    # GIVEN
    :param: import of the module

    # WHEN
    :param: add constraints on the functions to be tested

    # THEN
    :param: verification of the conformity of the test

    """

    def setUp(self) -> None:

        """ [NUMBER] """

        self.number_hosts_1 = 10
        self.number_hosts_2 = 50
        self.number_hosts_3 = 100

        self.number_subnet_1 = 2
        self.number_subnet_2 = 16
        self.number_subnet_3 = 128

        """ [CLASS IP] """

        self.class_ip_A = "A"
        self.class_ip_B = "B"
        self.class_ip_C = "C"

        """ [IP ADDRESS] """

        self.ip_address_str_A = "56.96.36.56"
        self.ip_address_str_B = "172.137.29.227"
        self.ip_address_str_C = "192.168.10.196"

        self.ip_address_decimal_A = [56, 96, 36, 56]
        self.ip_address_decimal_B = [172, 137, 29, 227]
        self.ip_address_decimal_C = [192, 168, 10, 196]

        self.ip_address_binary_A = [0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0]
        self.ip_address_binary_B = [1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1]
        self.ip_address_binary_C = [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 0, 0]

        """ [NETWORK] """

        self.network_str_A = "56.0.0.0"
        self.network_str_B = "172.137.0.0"
        self.network_str_C = "192.168.10.0"

        self.network_decimal_A = [56, 0, 0, 0]
        self.network_decimal_B = [172, 137, 0, 0]
        self.network_decimal_C = [192, 168, 10, 0]

        self.network_binary_A = [0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.network_binary_B = [1, 0, 1, 0, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.network_binary_C = [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        """ [SUBNET] """

        self.cidr_A = 8
        self.cidr_B = 16
        self.cidr_C = 24

        self.subnet_str_A = "255.0.0.0"
        self.subnet_str_B = "255.255.0.0"
        self.subnet_str_C = "255.255.255.0"

        self.subnet_decimal_A = [255, 0, 0, 0]
        self.subnet_decimal_B = [255, 255, 0, 0]
        self.subnet_decimal_C = [255, 255, 255, 0]

        self.subnet_binary_A = [1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.subnet_binary_B = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.subnet_binary_C = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]

        """ [NEW SUBNET] """

        self.new_subnet_decimal_A = [255, 240, 0, 0]
        self.new_subnet_decimal_B = [255, 255, 240, 0]
        self.new_subnet_decimal_C = [255, 255, 255, 240]

        self.new_subnet_binary_A = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.new_subnet_binary_B = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        self.new_subnet_binary_C = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0]

    """ [FUNCTION SAY] """

    def test_say_ip_address(self) -> None:

        #  GIVEN - WHEN - THEN

        self.assertListEqual(self.say(self.ip_address_str_A), self.ip_address_decimal_A)
        self.assertListEqual(self.say(self.ip_address_str_B), self.ip_address_decimal_B)
        self.assertListEqual(self.say(self.ip_address_str_C), self.ip_address_decimal_C)

        self.assertListEqual(self.say(self.network_str_A), self.network_decimal_A)
        self.assertListEqual(self.say(self.network_str_B), self.network_decimal_B)
        self.assertListEqual(self.say(self.network_str_C), self.network_decimal_C)

        self.assertListEqual(self.say(self.subnet_str_A), self.subnet_decimal_A)
        self.assertListEqual(self.say(self.subnet_str_B), self.subnet_decimal_B)
        self.assertListEqual(self.say(self.subnet_str_C), self.subnet_decimal_C)

    """ [FUNCTION CONVERT] """

    def test_convert_decimal_binary(self) -> None:

        #  GIVEN - WHEN - THEN

        self.assertEqual(self.convert_decimal_binary(self.ip_address_decimal_A), self.ip_address_binary_A)
        self.assertEqual(self.convert_decimal_binary(self.ip_address_decimal_B), self.ip_address_binary_B)
        self.assertEqual(self.convert_decimal_binary(self.ip_address_decimal_C), self.ip_address_binary_C)

        self.assertEqual(self.convert_decimal_binary(self.network_decimal_A), self.network_binary_A)
        self.assertEqual(self.convert_decimal_binary(self.network_decimal_B), self.network_binary_B)
        self.assertEqual(self.convert_decimal_binary(self.network_decimal_C), self.network_binary_C)

        self.assertEqual(self.convert_decimal_binary(self.subnet_decimal_A), self.subnet_binary_A)
        self.assertEqual(self.convert_decimal_binary(self.subnet_decimal_B), self.subnet_binary_B)
        self.assertEqual(self.convert_decimal_binary(self.subnet_decimal_C), self.subnet_binary_C)

    def test_convert_binary_decimal(self) -> None:

        #  GIVEN - WHEN - THEN

        self.assertEqual(self.convert_binary_decimal(self.ip_address_binary_A), self.ip_address_decimal_A)
        self.assertEqual(self.convert_binary_decimal(self.ip_address_binary_B), self.ip_address_decimal_B)
        self.assertEqual(self.convert_binary_decimal(self.ip_address_binary_C), self.ip_address_decimal_C)

    """ [FUNCTION DEFINE] """

    def test_define_network(self) -> None:

        #  GIVEN - WHEN - THEN

        self.assertEqual(self.define_network(self.ip_address_decimal_A, self.subnet_decimal_A), self.network_decimal_A)
        self.assertEqual(self.define_network(self.ip_address_decimal_B, self.subnet_decimal_B), self.network_decimal_B)
        self.assertEqual(self.define_network(self.ip_address_decimal_C, self.subnet_decimal_C), self.network_decimal_C)

    def test_define_ip_class(self) -> None:

        #  GIVEN - WHEN - THEN

        self.assertEqual(self.define_class(self.network_binary_A), self.class_ip_A)
        self.assertEqual(self.define_class(self.network_binary_B), self.class_ip_B)
        self.assertEqual(self.define_class(self.network_binary_C), self.class_ip_C)

    def test_define_cidr(self):
        pass

    def test_define_number_bits_host_id(self) -> None:
        pass

    def test_define_number_bits_net_id(self) -> None:
        pass

    def test_new_subnet_mask(self) -> None:
        pass


if __name__ == '__main__':
    unittest.main()
