__author__ = "BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.5"

from tests.ip_calculator import IpCalculator

module_ip = IpCalculator()


class IPCalculator:
    def __init__(self):
        self.ip_address = say_ip_address
        self.subnet_mask = say_subnet_mask

    def say(self):
        return module_ip.say(self.ip_address, self.subnet_mask)


class DivisionNumberHosts:
    pass


""" [PROGRAM] """

display = """
    Welcome to IP CALCULATOR
    
    If you do not choose the subnet mask, it is automatically proposed by default !

    Choice an option :
    
    \t1 : Division by number of hosts
    \t2 : Division by number of network
    \t3 : Division by variable number of hosts
    \t0 : Exit
"""

choice = "99"

while choice != "0":
    choice = input(display)
    match choice:
        case "1":
            say_ip_address = "192.168.10.10"  # input("Can you say IP address : ")
            say_subnet_mask = "255.255.255.0"  # input("Can you say subnet mask : ")
            number_hosts = "25"  # int(input("Can you say subnet mask : "))

            print(IPCalculator().say())
        case "2":
            say_ip_address = module_ip.say(input("Can you say IP address : "))
            number_network = int(input("Can you say network : "))
        case "3":
            pass
