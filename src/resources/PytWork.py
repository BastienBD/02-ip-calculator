__author__ = "BARABITE-DUBOUCH Bastien"
__copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
__license__ = "Private"
__version__ = "0.5"

import itertools
import re


class IP:
    pass


class IPv4:
    """ [IPv4]

    Module that allow to process @IPv4

    :param: an IPv4 address
    :param: ip address size = 4 bytes
    :param: byte size = 8 bits

    FUNCTION LIST:

        :list: say(add_ip_address)

        :list: define_network(add_ip_address, add_subnet_mask)
        :list: define_ip_class(add_network_binary)
        :list: define_cidr(add_subnet_mask_binary)
        :list: define_number_bits(add_number_necessary)

        :list: new_subnet_mask(add_subnet_mask_binary, add_number_bits_network_id)

        :list: generate_network_id(add_ip_address_binary, add_cidr, add_number_bits)
        :list: generate_hosts_id(add_number_bits_hosts_id, add_define_network_list)

        :list: convert_decimal_binary(add_ip_address_decimal)
        :list: convert_binary_decimal(add_ip_address_binary)

        :list: coming

    """

    @staticmethod
    def say(add_ip_address):
        """ [FUNCTION SAY]

        Converts the str("192.168.10.196") and return list([192, 168, 10, 196])

        :param add_ip_address: "X.X.X.X"
        :type: str()
        :return: [X, X, X, X]
        :rtype: list[int]

        """

        pattern_1 = re.compile("([0-9]{1,3}\.){3}[0-9]{1,3}$")
        pattern_2 = re.compile("([0-9]{1,3}\.){3}[0-9]{1,3}/[0-9]{1,2}$")

        splitting = lambda byte: [int(byte) for byte in add_ip_address.split(".")]

        if re.match(pattern_1, add_ip_address):
            for value in splitting(add_ip_address):
                if 0 <= value <= 255:
                    return splitting(add_ip_address)

        elif re.match(pattern_2, add_ip_address):
            subnet_mask, add_ip_address = add_ip_address.split("/")[1], add_ip_address.split("/")[0]

            return splitting(add_ip_address), int(subnet_mask)

        else:
            return "Please, enter a valid IP address !"

    @staticmethod
    def convert_decimal_binary(add_ip_address_decimal):
        """ [FUNCTION CONVERT DECIMAL BINARY]

        Convert @IP to decimal format and return @IP to binary format

        :param add_ip_address_decimal: [192, 168, 10, 0]
        :type: list[int()]
        :return: [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0,
                  0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        :rtype: list[int()]

        """
        ip_address_binary = []

        for byte in add_ip_address_decimal:
            for bit in range(8):
                if byte - 2 ** (7 - bit) >= 0:
                    ip_address_binary.append(1)
                    byte -= 2 ** (7 - bit)
                else:
                    ip_address_binary.append(0)

        return ip_address_binary

    @staticmethod
    def convert_binary_decimal(add_ip_address_binary):
        """ [FUNCTION CONVERT BINARY DECIMAL]

        Convert @IP to binary format and return @IP to decimal format

        :param add_ip_address_binary: [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0,
                                       0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]
        :type: list
        :return: [192, 168, 10, 0]
        :rtype: list

        """
        ip_address_decimal = []
        cutting = lambda binary_address, bits: [binary_address[i: i + bits] for i in range(0, len(binary_address), bits)]

        for byte in cutting(add_ip_address_binary, 8):
            intermediate_variable = 0
            for bit in range(len(byte)):
                if byte[::-1][bit] == 1:
                    intermediate_variable += 2 ** bit

            ip_address_decimal.append(intermediate_variable)

        return ip_address_decimal

    @staticmethod
    def define_network(add_ip_address, add_subnet_mask):
        """ [FUNCTION DEFINE NETWORK]

        Performs a LOGICAL AND between the @IP and the @subnet-mask and return the @network

        :param add_ip_address: [192, 168, 10, 196]
        :param add_subnet_mask: [255, 255, 255, 0]
        :type add_ip_address: list[int]
        :type add_subnet_mask: list[int]
        :return: [192, 168, 10, 0]
        :rtype: list[int]

        """
        network_address = []

        for byte in range(len(add_ip_address)):
            network_address.append(add_ip_address[byte] & add_subnet_mask[byte])

        return network_address

    @staticmethod
    def define_ip_class(add_network_binary):
        """ [FUNCTION DEFINE IP CLASS]

        Compares the most significant bits (MSB) of the @network and return the class of the @network

        :param add_network_binary: [192, 168, 10, 0]
        :type: list[int]
        :return: ["C"]
        :rtype: list[str]

        """
        class_table = {"A": [0],
                       "B": [1, 0],
                       "C": [1, 1, 0],
                       "D": [1, 1, 1, 0],
                       "E": [1, 1, 1, 1, 0]}

        for enumerated, (key, value) in enumerate(class_table.items()):
            if value == add_network_binary[:len(value)]:
                return key

# [A VERIFIER]

    @staticmethod
    def define_cidr(add_subnet_mask_binary):
        """ [FUNCTION DEFINE CIDR]

        Get all the continuous sequence of bits to "1" and return the sum

        :param add_subnet_mask_binary: [255, 255, 255, 0]
        :type: list[int]
        :return: 24
        :rtype: int()

        """
        return sum(bit for bit in add_subnet_mask_binary if bit == 1)

    @staticmethod
    def define_number_bits(add_number_necessary):
        """ [FUNCTION DEFINE NUMBER BITS]

        Get the number of "hosts" or "network" and return the number of bits necessary

        :param add_number_necessary: 100
        :type: int()
        :return: 7
        :rtype: int()

        """
        number_bits, exhibitor = 0, 0

        while exhibitor < add_number_necessary:
            number_bits += 1
            exhibitor = 2 ** number_bits

        return number_bits

    @staticmethod
    def new_subnet_mask(add_subnet_mask_binary, add_number_bits_network_id):
        """ [FUNCTION NEW SUBNET MASK]

        Get the subnet-mask and the number of bits in the network_id and return the new subnet-mask

        :param add_subnet_mask_binary: [255, 255, 255, 0]
        :param add_number_bits_network_id: 1
        :type add_subnet_mask_binary: list[int]
        :type add_number_bits_network_id: int()
        :return: [255, 255, 255, 128]
        :rtype: list[int]

        """
        for value in range(len(add_subnet_mask_binary)):
            if add_number_bits_network_id > 0:
                if add_subnet_mask_binary[value] == 0:
                    add_subnet_mask_binary[value] = 1
                    add_number_bits_network_id -= 1

        return add_subnet_mask_binary

    @staticmethod
    def generate_network_id(add_ip_address_binary, add_cidr, add_number_bits):
        return [add_ip_address_binary[:add_cidr] + [int(bit) for bit in change] for change in
                map("".join, itertools.product("01", repeat=add_number_bits))]

    @staticmethod
    def generate_hosts_id(add_number_bits_hosts_id: int, add_define_network_list: list[list[int]]) -> list[list[list[int]]]:
        return [[address_binary + [int(bit) for bit in change] for change in map("".join, itertools.product("01", repeat=add_number_bits_hosts_id))]
                for address_binary in add_define_network_list]

    """ A FAIRE """

    def distributed_ip_address(self, add_new_addressing_binary):
        network_binary, first_ip_address_binary, last_ip_address_binary, broadcast_address_binary = [], [], [], []

        for ip_address in add_new_addressing_binary:
            network_binary.append(ip_address[0])
            first_ip_address_binary.append(ip_address[1])
            last_ip_address_binary.append(ip_address[-2])
            broadcast_address_binary.append(ip_address[-1])

        network_decimal = self.convert_binary_decimal(network_binary)
        first_ip_address_decimal = self.convert_binary_decimal(first_ip_address_binary)
        last_ip_address_decimal = self.convert_binary_decimal(last_ip_address_binary)
        broadcast_address_decimal = self.convert_binary_decimal(broadcast_address_binary)

        for address in range(len(network_decimal)):
            return """"
                Network: {}
                Network address: {}.{}.{}.{}
                First host address: {}.{}.{}.{}
                Last host address: {}.{}.{}.{}
                Broadcast address: {}.{}.{}.{}
                Subnet-mask address: {}.{}.{}.{}
                """.format(
                address + 1,
                *network_decimal[address],
                *first_ip_address_decimal[address],
                *last_ip_address_decimal[address],
                *broadcast_address_decimal[address],
                sep="."
            )


class IPv6:
    pass
