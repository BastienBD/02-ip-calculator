<br/>- __author__ = "BARABITE-DUBOUCH Bastien"
<br/>- __copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
<br/>- __license__ = "Protected"
<br/>- __version__ = "0.5"

<div align="center">
    <br/>
    <img src="https://media2.giphy.com/media/KAq5w47R9rmTuvWOWa/giphy.gif?cid=790b761143ba0f191bc4892763d351de35639f203b8ecdd8&rid=giphy.gif&ct=g" height="200">
    <br/>
    <h1>IP CALCULATOR</h1>
    <br/>
    <p align="center">
        <a href="https://www.codacy.com/gl/BastienBD/02-ip-calculator/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=BastienBD/02-ip-calculator&amp;utm_campaign=Badge_Grade"><img src="https://app.codacy.com/project/badge/Grade/142af63292fe4cb081dfbadcc7a14fc9" height="25"></a>
        <a href="https://www.python.org/downloads/release/python-3100/"><img src="https://img.shields.io/badge/python-3.10-blue.svg" height="25"></a>
    </p>
</div>

<p align="center">
    Ce projet 
</p>

# Index

1. [Configuration](#configuration)
2. [Description du contexte](#description-du-contexte)
3. [Feuille de route](#feuille-de-route)
    1. [Itération 1](#iteration-1)
    2. [Itération 2](#iteration-2)
    3. [Itération 3](#iteration-3)
    4. [Démonstration](#demonstration)
4. [Usage](#usage)
    1. [Auteurs](#auteurs)
    2. [Licence](#licence)
    3. [Debug](#debug)
5. [Crédit](#credit)

# Configuration

<br/>

Le projet utilise la dernière version de Python (Python 3.10),
Vous pouvez trouver la liste des modules avec toutes les options dans le fichier : requirements.txt.

<br/>

# Description du contexte

<br/>

Créez une calculatrice qui calcule les sous-réseaux à partir des données fournies par l'utilisateur :

    - nombre d'hôtes,
    - nombre de sous-réseaux,
    - nombre variable d'hôtes.

<br/>

## Qu'est ce qu'une adresse IP

Une adresse <b>IP</b> (<b>Internet Protocol</b>) est un numéro d'identification qui est attribué de façon permanente ou provisoire à chaque périphérique relié à un réseau informatique qui utilise l'Internet Protocol.

<div align="center">
    <a href="https://fr.wikipedia.org/wiki/Adresse_IP"><img src="https://upload.wikimedia.org/wikipedia/commons/3/34/Adresse_Ipv4.svg" height="300"></a>
</div>


: il s'agit d'une série de 4 octets, qui sont séparés par des "." pour faciliter leur lecture. Chacun de ces octets est composé de 8 bits, donc permet de coder 256 valeurs différentes, réparties de 0 à 255.

classe d'adresse IP :

## Qu'est ce qu'un réseau ?


# Pourquoi le découpage réseaux ?

## Qu'est ce que le découpage VLSM ?


# Feuille de route

<br/>

La feuille de route est divisée en plusieurs itérations afin d'avancer dans le programme étape par étape. La méthode TDD est utilisée jusqu'à l'itération 3.

<br/>

# ARBO

src
    resources
        PytWork.py
    tests
        ip_calculator.py
    main.py
    qualification.py

PytWork.py :

class IPv4:
        :list: say(add_ip_address)

        :list: define_network(add_ip_address, add_subnet_mask)
        :list: define_ip_class(add_network_binary)
        :list: define_ip_status(add_ip_address)

        :list: define_cidr(add_subnet_mask_binary)
        :list: define_number_bits(add_number_necessary)

        :list: new_subnet_mask(add_subnet_mask_binary, add_number_bits_network_id)

        :list: generate_network_id(add_ip_address_binary, add_cidr, add_number_bits)
        :list: generate_hosts_id(add_number_bits_hosts_id, add_define_network_list)

        :list: convert_decimal_binary(add_ip_address_decimal)
        :list: convert_binary_decimal(add_ip_address_binary)

        :list: coming


## Itération 1

```
    - Get @ip: (ex: "192.168.10.196": str()) -> list[int]
    - Get @mask: (ex: "255.255.255.0": str()) -> list[int]

    - Return the @network: (ex: 192.168.10.0: list[int])

    - Convert @network from decimal(10) to binary(2): (ex: 192.168.10.0: list[int]) -> list[int]
    - Convert @mask from decimal(10) to binary(2): (ex: 255.255.255.0: list[int]) -> list[int]

    - Return @network in binary form: (ex: [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]: list[int])
    - Return @mask in binary form: (ex: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]: list[int])
```

## Iteration 2:

```
    - Get the number of hosts you want: (ex: 100: int)
    - Get the number of subnets you want: (ex: 2: int)

    - Caculate the number of bits needed in the host_id and net_id parts

    - Return the number of bits needed in the host_id and net_id part compared to the number of hosts desired: (ex: host_id = 7: int, net_id = 1: int)
    - Return the number of bits needed in the host_id and net_id part compared to the number of subnets desired: (ex: host_id = 7: int, net_id = 1: int)
```

## Demo

```
    Choice an option:
    	1 : Division by number of hosts
    	2 : Division by number of subnets
    	3 : Division by variable number of hosts
    	0 : Exit
    >? 1
Can you say IP Address : >? 192.168.10.196
Can you say Subnet mask : >? 255.255.255.0
Number hosts desired : >? 100
                  Network: 1
                  Network address: 192.168.10.0
                  First host address: 192.168.10.1
                  Last host address: 192.168.10.126
                  Broadcast address: 192.168.10.127
                  Subnet-mask address: 255.255.255.128
                  
                  Network: 2
                  Network address: 192.168.10.128
                  First host address: 192.168.10.129
                  Last host address: 192.168.10.254
                  Broadcast address: 192.168.10.255
                  Subnet-mask address: 255.255.255.128
                  
    Choice an option:
    	1 : Division by number of hosts
    	2 : Division by number of subnets
    	3 : Division by variable number of hosts
    	0 : Exit
    >? 2
Can you say IP Address : >? 192.168.10.196
Can you say Subnet mask : >? 255.255.255.0
Number network desired : >? 2
 Network: 1
                  Network address: 192.168.10.0
                  First host address: 192.168.10.1
                  Last host address: 192.168.10.126
                  Broadcast address: 192.168.10.127
                  Subnet-mask address: 255.255.255.128
                  
 Network: 2
                  Network address: 192.168.10.128
                  First host address: 192.168.10.129
                  Last host address: 192.168.10.254
                  Broadcast address: 192.168.10.255
                  Subnet-mask address: 255.255.255.128
                  
    Choice an option:
    	1 : Division by number of hosts
    	2 : Division by number of subnets
    	3 : Division by variable number of hosts
    	0 : Exit
```

# Usage

This project was done in order to improve my programming skills and facilitate my work as a pro network DevOps.

## Auteurs

    - BARABITE-DUBOUCH Bastien alias @BastienBD

## Licence

Ce projet est soumis à la licence GNU General Public License v3.0 https://gitlab.com/BastienBD/02-ip-calculator/-/blob/main/LICENCE.md

Toute reproduction, représentation, modification, publication, adaptation de toute ou partit des éléments du programme quel que soit le moyen ou le procédé utilisé, est autorisé mais soumis à l'identification du créateur et de ces collaborateurs.

## Debugging


# Credits

<div align="center">
    L'idée et l'approche ont été adaptées de la solution proposé en JavaScript par Sebastien ADAM :<a href="https://www.sebastienadam.be/ipcalculator/"><img src="https://www.sebastienadam.be/images/ip_calculator_001.png"></a>
</div>

<div align="center"><h1>Conclusion</h1></div>

<div align="center">
    <br/>Toute modification partielle ou complète du code source peut endommager le programme et entraîner la corruption ou la perte des données.
    <br/>Pour toutes informations complémentaire vous pouvez me contacter via le portefolio : <a href="https://btssiobarabitedubouchbastien.wordpress.com/">My Portfolio</a>
    <br/>En cas de problème technique, toute question concernant le programme ou si vous constatez un problème, merci de vous adresser directement à moi via :
</div>

<br/>

<div align="center"><p>@: bastien.barabitedubouch18@gmail.com</p></div>

<br/>

<div align="center">
    <a href="#IP-CALCULATOR"><img src="http://randojs.com/images/backToTopButton.png" title="Haut de page" height="30"></a>
</div>

<br/>

<div align="center">
    <p>
        <a href="https://btssiobarabitedubouchbastien.wordpress.com/" class="fancybox"><img src="https://user-images.githubusercontent.com/63207451/127334786-f48498e4-7aa1-4fbd-b7b4-cd78b43972b8.png" title="Profil" width="38" height="38"></a>
        <a href="https://gitlab.com/BastienBD" class="fancybox" ><img src="https://user-images.githubusercontent.com/63207451/97302854-e484da80-1859-11eb-9374-5b319ca51197.png" title="GitHub" width="40" height="40"></a>
        <a href="https://www.linkedin.com/in/barabite-dubouch-bastien/" class="fancybox" ><img src="https://user-images.githubusercontent.com/63207451/97303444-b2c04380-185a-11eb-8cfc-864c33a64e4b.png" title="LinkedIn" width="40" height="40"></a>
        <a href="mailto:bastien.barabitedubouch18@gmail.com" class="fancybox" ><img src="https://user-images.githubusercontent.com/63207451/97303543-cec3e500-185a-11eb-8adc-c1364e2054a9.png" title="Mail" width="40" height="40"></a>
    </p>
</div>