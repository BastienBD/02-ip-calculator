<br/>__author__ = "BARABITE-DUBOUCH Bastien"
<br/>__copyright__ = "Copyright (C) 2021 BARABITE-DUBOUCH Bastien"
<br/>__license__ = "Private"
<br/>__version__ = "0.5"

</br>

<p align="center">
    <img src="https://media2.giphy.com/media/KAq5w47R9rmTuvWOWa/giphy.gif?cid=790b761143ba0f191bc4892763d351de35639f203b8ecdd8&rid=giphy.gif&ct=g" height="200">
</p>

<h1 align="center">IP CALCULATOR</h1>

<p align="center">
    <a href="https://www.codacy.com/gl/BastienBD/02-ip-calculator/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=BastienBD/02-ip-calculator&amp;utm_campaign=Badge_Grade"><img src="https://app.codacy.com/project/badge/Grade/142af63292fe4cb081dfbadcc7a14fc9" height="25"></a>
    <a href="https://www.python.org/downloads/release/python-3100/"><img src="https://img.shields.io/badge/python-3.10-blue.svg" height="25"></a>
</p>

# Index

# Description of the context:

Create a calculator that calculates subnets based on user-supplied data:

    - number of hosts
    - number of subnets
    - variable number of hosts

## Setup

The project uses the last version of Python (Python 3.10).
You will find the list of packages with all the options in a file requirements.txt

# Road map:

The roadmap is divided into several iterations in order to advance in the program step by step. The TDD method was used until iteration 3.

## Iteration 1:

```
    - Get @ip: (ex: "192.168.10.196": str()) -> list[int]
    - Get @mask: (ex: "255.255.255.0": str()) -> list[int]

    - Return the @network: (ex: 192.168.10.0: list[int])

    - Convert @network from decimal(10) to binary(2): (ex: 192.168.10.0: list[int]) -> list[int]
    - Convert @mask from decimal(10) to binary(2): (ex: 255.255.255.0: list[int]) -> list[int]

    - Return @network in binary form: (ex: [1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0]: list[int])
    - Return @mask in binary form: (ex: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0]: list[int])
```

## Iteration 2:

```
    - Get the number of hosts you want: (ex: 100: int)
    - Get the number of subnets you want: (ex: 2: int)

    - Caculate the number of bits needed in the host_id and net_id parts

    - Return the number of bits needed in the host_id and net_id part compared to the number of hosts desired: (ex: host_id = 7: int, net_id = 1: int)
    - Return the number of bits needed in the host_id and net_id part compared to the number of subnets desired: (ex: host_id = 7: int, net_id = 1: int)
```

## Demo

```
    Choice an option:
    	1 : Division by number of hosts
    	2 : Division by number of subnets
    	3 : Division by variable number of hosts
    	0 : Exit
    >? 1
Can you say IP Address : >? 192.168.10.196
Can you say Subnet mask : >? 255.255.255.0
Number hosts desired : >? 100
                  Network: 1
                  Network address: 192.168.10.0
                  First host address: 192.168.10.1
                  Last host address: 192.168.10.126
                  Broadcast address: 192.168.10.127
                  Subnet-mask address: 255.255.255.128
                  
                  Network: 2
                  Network address: 192.168.10.128
                  First host address: 192.168.10.129
                  Last host address: 192.168.10.254
                  Broadcast address: 192.168.10.255
                  Subnet-mask address: 255.255.255.128
                  
    Choice an option:
    	1 : Division by number of hosts
    	2 : Division by number of subnets
    	3 : Division by variable number of hosts
    	0 : Exit
    >? 2
Can you say IP Address : >? 192.168.10.196
Can you say Subnet mask : >? 255.255.255.0
Number network desired : >? 2
 Network: 1
                  Network address: 192.168.10.0
                  First host address: 192.168.10.1
                  Last host address: 192.168.10.126
                  Broadcast address: 192.168.10.127
                  Subnet-mask address: 255.255.255.128
                  
 Network: 2
                  Network address: 192.168.10.128
                  First host address: 192.168.10.129
                  Last host address: 192.168.10.254
                  Broadcast address: 192.168.10.255
                  Subnet-mask address: 255.255.255.128
                  
    Choice an option:
    	1 : Division by number of hosts
    	2 : Division by number of subnets
    	3 : Division by variable number of hosts
    	0 : Exit
```

## Usage

This project was done in order to improve my programming skills and facilitate my work as a pro network DevOps.

## Auteurs

    - BARABITE-DUBOUCH Bastien alias @BastienBD

## Licence

This project is licensed under the GNU General Public License v3.0 https://gitlab.com/BastienBD/02-ip-calculator/-/blob/main/LICENCE.md

## Debugging


## Credits

The idea and approach was adapted from https://www.sebastienadam.be/ipcalculator/

![] https://www.sebastienadam.be/images/ip_calculator_001.png
